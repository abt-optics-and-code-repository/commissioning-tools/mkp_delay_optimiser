import numpy as np
import pyjapc
import time

japc = pyjapc.PyJapc(selector="SPS.USER.LHCINDIV", incaAcceleratorName='SPS', noSet=True)

#Process variable for timing the subscription wit cycle stamps 
#subscribe_vars = "XTS.INJAMC-XTIM/Acquisition"
        
#def test_callback(subscribe_vars, dataReceived):
#    print("\nNew value for parameter {} acqC is: {}".format(subscribe_vars, dataReceived))

def callb(param, data):
    print(param, data)

#Start the subscription of the callback function 
japc.subscribeParam("XTS.INJAMC-XTIM/Acquisition", onValueReceived=callb)


japc.startSubscriptions()

#while True:
#    time.sleep(1)

"""
New value for parameter ['XTS.INJAMC-XTIM/Acquisition'] acqC is: 1015

New value for parameter ['XTS.INJAMC-XTIM/Acquisition'] acqC is: 4615
0 [JapcExecutor-thread-3] WARN cern.japc.core.spi.group.PushGroupSubscriptionStrategy  - An update for the cycleStamp 1649177329335000000 for parameter XTS.INJAMC-XTIM/Acquisition came too late. The next update is already being processed!
3597 [JapcExecutor-thread-4] WARN cern.japc.core.spi.group.PushGroupSubscriptionStrategy  - An update for the cycleStamp 1649177329335000000 for parameter XTS.INJAMC-XTIM/Acquisition came too late. The next update is already being processed!

New value for parameter ['XTS.INJAMC-XTIM/Acquisition'] acqC is: 1015
34797 [JapcExecutor-thread-6] WARN cern.japc.core.spi.group.PushGroupSubscriptionStrategy  - An update for the cycleStamp 1649177367735000000 for parameter XTS.INJAMC-XTIM/Acquisition came too late. The next update is already being processed!
38397 [JapcExecutor-thread-7] WARN cern.japc.core.spi.group.PushGroupSubscriptionStrategy  - An update for the cycleStamp 1649177367735000000 for parameter XTS.INJAMC-XTIM/Acquisition came too late. The next update is already being processed!
41998 [JapcExecutor-thread-8] WARN cern.japc.core.spi.group.PushGroupSubscriptionStrategy  - An update for the cycleStamp 1649177367735000000 for parameter XTS.INJAMC-XTIM/Acquisition came too late. The next update is already being processed!
"""
