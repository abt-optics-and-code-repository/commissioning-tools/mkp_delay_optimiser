import numpy as np
import pyjapc

japc = pyjapc.PyJapc(selector="SPS.USER.LHCINDIV", incaAcceleratorName="SPS")

def callb(param, data):
    print(param, data)

japc.subscribeParam("XTS.INJAMC-XTIM/Acquisition", onValueReceived=callb)

japc.startSubscriptions()
