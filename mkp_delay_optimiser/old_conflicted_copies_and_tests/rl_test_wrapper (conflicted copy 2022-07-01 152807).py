"""
RL test wrapper to create fake MKP delays environment, testing out an RL agent with Stable Baselines 3
"""

from stable_baselines3 import PPO, A2C # DQN coming soon
from stable_baselines3.common.env_util import make_vec_env

from stable_baselines3.common.env_checker import check_env
from mkpdelays_env_RL import MKPOptEnv

env =  MKPOptEnv(use_surrogate=True)

# If the environment don't follow the interface, an error will be thrown
#check_env(env, warn=True)  # --> for now works without error messages 

# Perform a reset command
obs = env.reset()

# wrap the environment 
env = make_vec_env(lambda: env, n_envs=1)

env.reset()
env.render()

"""
# Train the agent
model = A2C('MlpPolicy', env, verbose=1).learn(500)

# Test the trained agent
obs = env.reset()
n_steps = 20
for step in range(n_steps):
  action, _ = model.predict(obs, deterministic=True)
  print("Step {}".format(step + 1))
  print("Action: ", action)
  obs, reward, done, info = env.step(action)
  print('obs=', obs, 'reward=', reward, 'done=', done)
"""