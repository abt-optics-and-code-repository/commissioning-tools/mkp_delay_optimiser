import numpy as np
import pandas as pd
import pickle
from pathlib import Path
import sys
from cern_general_devices.mkp_delays import MKPdelays
from cern_general_devices.bpm import BPMLHC  # BPM device, normally for LHC
from cern_general_devices.bct import BCTDC  # beam intensity device
import logging
import time


class RealMKPdelays:
    def __init__(
        self, japc, limits, mkp_index, bunch_index_width, canc_token=None
    ):

        self.japc = japc
        self.limits = limits
        self.mkp_index = mkp_index
        self.bunch_index_width = bunch_index_width
        self.token = canc_token
        self.mkp_delays = MKPdelays(self.japc, self.limits, self.mkp_index)
        self.time_data = np.genfromtxt(
            "data_and_sequences/sps_lhcpilot_time_data.txt"
        )  # same MKP waveform time data for all cycles
        self.bpm = BPMLHC(self.japc)  # bpm of LHC type
        self.n_turns = self.bpm.n_turns  # number of turns bpm is looking at
        self.bct = BCTDC(self.japc)  # beam intensity monitor
        self.V_MKP_min = (
            1.5  # minimum voltage for the MKP waveform, now just test value
        )

        self.valid_cycle = False
        self.bpm_pos = 0.0
        self.count_iterations = 0
        self.bpm_pos_ref = 0.0
        self.bpm_pos_all = []
        self.token = canc_token

        # Iterate over the waveform data variables
        mkps_IDs = [
            "1A",
            "1B",
            "2A",
            "2B",
            "3A",
            "3B",
            "4A",
            "4B",
            "5A",
            "5B",
            "6A",
            "6B",
            "7A",
            "7B",
            "8A",
            "8B",
        ]
        self.kick_data_vars = []
        for ele in mkps_IDs:
            self.kick_data_vars.append(
                "MKP.BA1.IPOC.TMR{}/Waveform#waveformData".format(ele)
            )

        # Define process variables for horizontal BPM position and for extracted intensity
        self.all_vars = [
            self.bpm.data_vars[
                0
            ],  # horizontal beam position, if vertical also desired then this is index=1
            self.bct.bct5_var + "/Acquisition#dumpInt",  # intensity
        ]

        # Add the MKP waveform data variables
        for ele in self.kick_data_vars:
            self.all_vars.append(ele)

        # Process variable for timing the subscription wit cycle stamps
        self.subscribe_vars = "XTS.INJAMC-XTIM/Acquisition"

        # Start the subscription of the callback function
        self.japc.subscribeParam(
            self.subscribe_vars, self._callback, getHeader=True
        )
        self.japc.startSubscriptions()

    # Callback method for parameter subscription
    def _callback(self, parameter_name, data_received, header):
        if header["isFirstUpdate"]:
            print(
                "Starting subscription with callback function...\n"
            )  # skip the automatic first acquisition
        else:
            self.valid_cycle = False

            # IF DESIRED, PRINT OUT THE VALUES FROM THE SUBSCRIPTION PARAMETERS WHEN CALLBACK IS USED
            # print("\n\nData received from subscriptions: {}\n\n".format(data_received))

            self.acqC = data_received["acqC"]
            print("\nacqC is {}\n".format(self.acqC))
            if (
                self.acqC != 3415
            ):  # ensure that we are on injection, check that acquisition cycle is correct
                logging.info("Waiting for next cycle to start...")
                print("\nWaiting for next cycle to start...\n")
            else:
                logging.info("On injection! Attempt to extract data...")
                print("\nOn injection! Attempt to extract data...\n")
                time.sleep(0.5)  # wait until 2nd injection
                self.data_mkp_all = self.mkp_delays.get_all_mkp_para()
                self.mkp_kick_data = []
                for ele in self.kick_data_vars:
                    mkp_test = self.japc.getParam(ele)
                    self.mkp_kick_data.append(mkp_test)

                logging.info("Waveform data collected.")
                self.int_dumped_now = self.bct.get_dumped_intensity()
                logging.info("I-out = {:.2e}".format(self.int_dumped_now))

                # Extract BPM data, then find the indices of correct rows corresponding to the injected and circulating beam
                self.bpm_data = self.bpm.getParameter()
                df = pd.DataFrame(
                    self.bpm_data[0]["hor"][1]
                )  # horizontal BPM data of second BPM (SPS.BPMB.51503) --> matrix of turns and bunches
                df = df.loc[
                    :, (df != 0).any(axis=0)
                ]  # remove all zero-valued columns, where there is no injected nor circulating beam
                # Now check if injected beam is missing (one column) or if both circulating and injected are missing (both columns missing), or if BPM data is missing for some turns
                if df.empty:
                    print("\nEmpty BPM positions, waiting...\n")
                else:
                    if (
                        len(df.columns) == 1
                        or df.astype(bool).sum(axis=0).iloc[0] != self.n_turns
                    ):
                        print(
                            "\nInjected or circulating beam lost - adding penalty...\n"
                        )
                        x1 = np.zeros(self.n_turns)
                        x1[
                            0
                        ] = 500.0  # set first bpm value to something high to simulate large first oscillation to give high penalty
                        x2 = np.zeros(self.n_turns)
                        x2[0] = 50.0
                    else:
                        waveslice = np.array(
                            df.iloc[0, :]
                        )  # projection of the waveforms, first row

                        # Find the indices of correct rows corresponding to the injected and circulating beam
                        if self.bunch_index_width == 1:
                            x1 = np.array(df.iloc[:, 0])
                            x2 = np.array(df.iloc[:, 1])
                        else:
                            indices = waveslice.nonzero()[
                                0
                            ]  # find indices of non-zero elements
                            ind_start = indices[0]
                            ind1 = (
                                ind_start + (self.bunch_index_width - 1)  #the last one 
                        )  # if waveform index width is know, pick the last one
                        ind2 = ind1 + 1 #simply the index after ind1 



                            ind1 = (
                                ind_start + self.bunch_index_width
                            )  # if waveform index width is know, pick the last one
                            ind2 = indices[np.where(indices > ind1)][
                                0
                            ]  # second index simply the next value of "indices", larger than ind1


                            # bpm position vectors for injected and circulating
                            x1 = np.array(df.iloc[:, ind1])
                            x2 = np.array(df.iloc[:, ind2])

                    self.bpm_pos_now = np.array([x1, x2])
                    logging.info(
                        "Horizontal max beam positions: {} and {}".format(
                            np.max(self.bpm_pos_now[0]),
                            np.max(self.bpm_pos_now[1]),
                        )
                    )
                    print(
                        "Horizontal max beam positions: {} and {}".format(
                            np.max(self.bpm_pos_now[0]),
                            np.max(self.bpm_pos_now[1]),
                        )
                    )

                    # Ensure that intensity and MKP waveform currents of 2nd and last kicker magnets are high enough
                    logging.info(
                        "First MKP switch max flat top voltage: {:.2f}".format(
                            np.max(self.mkp_kick_data[0])
                        )
                    )
                    print(
                        "First MKP switch max flat top voltage: {:.2f}".format(
                            np.max(self.mkp_kick_data[0])
                        )
                    )
                    if (
                        np.max(self.mkp_kick_data[1]) >= self.V_MKP_min
                        and np.max(self.mkp_kick_data[-1]) >= self.V_MKP_min
                    ):  # and self.int_dumped_now >= 1.5e11:
                        logging.info(self.int_dumped_now)
                        self.bpm_pos_all.append(self.bpm_pos_now)
                        logging.info(
                            "Beam extracted. I-out = {:.2e}".format(
                                self.int_dumped_now
                            )
                        )
                        print(
                            "Beam extracted. I-out = {:.2e}".format(
                                self.int_dumped_now
                            )
                        )
                        self.valid_cycle = True
                    else:
                        self.valid_cycle = False
                        logging.info(
                            "No beam extracted. I-out = {:.2e}".format(
                                self.int_dumped_now
                            )
                        )
                        print(
                            "No beam extracted. I-out = {:.2e}".format(
                                self.int_dumped_now
                            )
                        )

                time.sleep(0.5)

    # Method to continously ensure that correct cycle is running
    def _wait_good_cycle_and_get_bpm_pos(self):
        while True:
            # this checks if cancellation is clicked
            if self.token is not None:
                if self.token.cancellation_requested:
                    self.token.complete_cancellation()
                    self.token.raise_if_cancellation_requested()

            if self.valid_cycle:
                self.valid_cycle = False
                print("\n\nVALID CYCLE\n\n")
                return self.bpm_pos_now
            else:
                time.sleep(0.5)
                logging.info("waiting for good cycle...")
                print("Waiting for good cycle...")

    # Method to set parameters in japc iterating over tuples of desired settings and values
    def _set_actors(self, variables, settings):
        for variable, setting in zip(variables, settings):
            self.japc.setParam(variable, setting)

    # Method to get squared sum of average BPM positions returned for the _get_reward method in parent class
    def get_positions(self, dtau) -> float:
        self.valid_cycle = False
        self.t = dtau[-1]  # general time stamp for beam
        self.mkp_delays.set_all_mkp_para(dtau)
        bpm_pos = self._wait_good_cycle_and_get_bpm_pos()

        return bpm_pos
