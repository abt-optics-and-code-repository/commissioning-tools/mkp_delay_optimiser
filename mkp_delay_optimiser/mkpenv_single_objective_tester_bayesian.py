"""
Small script to test the compute_single_objective method of the mkpdelays_env.py with Bayesian optimisation 
"""

import matplotlib.pyplot as plt
import matplotlib.ticker
import numpy as np
from bayes_opt import BayesianOptimization
import pickle
import datetime
from pyjapc import PyJapc


from mkpdelays_env import MKPOptEnv

#------------- INITIALIZE JAPC, TRAINS PER BATCH AND PLOTSAVING ----------------------
japc = PyJapc(selector="SPS.USER.LHCINDIV", incaAcceleratorName='SPS', noSet=False)
bunch_index_width = 1  #default value 1, if no bunch train 
saveplot = True  #choose to save plots or not 
#-------------------------------------------------------------------

mkp_env = MKPOptEnv(japc, bunch_index_width=bunch_index_width)

print("\nx0 action is: {}\n".format(mkp_env.x0_action))
test_action = mkp_env.x0_action_norm
print("Normalised x0 action is: {}\n".format(test_action))
isokay = np.all(test_action >= -1) and np.all(test_action <= 1)
print("x0 action within limits: {}".format(isokay))

mkp_env.reset()

dtau0 = mkp_env.get_initial_params()
dof = len(dtau0)
print("\n\nInitial conditions from fist taken normalised action: tau0 = {}\n\n".format(dtau0))

iterations = 1
positions = np.zeros(iterations)
iter_needed = np.zeros(iterations)

opt_name = "bayes_new"
data_error = {}
total_data = []


TIMESTAMP = str(datetime.datetime.now()).replace(" ", "_").replace(":", "-")
#ROOT_SAVE = "/afs/cern.ch/user/e/elwaagaa/cernbox/mkp-waveforms/mkp_optimisation_real_sps_test/mkp-waveform-optimiser/mkp-waveform-optimiser/plots_and_data_bayesian/"
print("Timestamp is {}".format(TIMESTAMP))

fig, axis = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(11, 8))
axis[0].plot([], [])
axis[1].plot([], [])
axis[0].yaxis.label.set_size(16)
axis[0].tick_params(axis='y', labelsize=13)
axis[1].yaxis.label.set_size(16)
axis[1].xaxis.label.set_size(16)
axis[1].tick_params(axis='x', labelsize=13)
axis[1].tick_params(axis='y', labelsize=13)
legend_names = ['Switch 1', 'Switch 2', 'Switch 3', 'Switch 4', 'Switch 5', 'Switch 6', 'Switch 7', 'Switch 8', 'General shift']

actions_all = []
penalty = []

#Define function to minimise, which also generates plots and saves data 
def bayesian_objective(dtau1, dtau2, dtau3, dtau4, dtau5, dtau6, dtau7, dtau8, dtau9):

    actions = np.array([dtau1, dtau2, dtau3, dtau4, dtau5, dtau6, dtau7, dtau8, dtau9])
    reward = -1 * mkp_env.compute_single_objective(actions)
    #reward = 10.0      #test reward 

    print("\nREWARD IS {}\n".format(reward))
    penalty.append(-1*reward)
    actions_all.append(actions)

    #Serialize and save data 
    if saveplot:
        with open("plots_and_data_bayesian/actors_mkp_delays_bayesian_log_data_{}.pickle".format(TIMESTAMP), "wb") as handle:
            pickle.dump(mkp_env.log_data, handle, protocol=pickle.HIGHEST_PROTOCOL)

    for ax in axis:
        ax.cla()
    axis[0].plot(penalty, marker=10, ms=12)
    axis[0].set(ylabel="Sq sum BPM positions [mm]")
    axis[0].set_yscale('log')
    #axis[0].set_yticks([3.0, 5.0, 8.0])  #add extra ticks of BPM positions
    #axis[0].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    axis[1].plot(actions_all, label=legend_names, marker=11, ms=12)
    axis[1].set(xlabel="#Iterations", ylabel="Actions")
    axis[1].legend(loc='right', fancybox=True, bbox_to_anchor=(1.125, 0.6))
    plt.subplots_adjust(wspace=0, hspace=0)
    fig.canvas.draw()
    axis[0].get_shared_x_axes().join(axis[0], axis[1])  #make the subplots share the x-axis of iterations 
    if saveplot:
        fig.savefig('plots_and_data_bayesian/mkp_delays_actors_bayesian_plot_{}.png'.format(TIMESTAMP), dpi=250)
    plt.pause(0.001)
    
    return reward


#Bayesian optimisation 
p_bounds = {f"dtau{ele}": (-1, 1) for ele in range(1, dof+1)}
optimiser = BayesianOptimization(
    f=bayesian_objective,
    pbounds=p_bounds,
    verbose=1,
    random_state=10,
)
# init-point = random exploration
# n_iter = interation of gaussian process
#hyperparameters can be tuned from this example: https://github.com/fmfn/BayesianOptimization/blob/master/examples/exploitation_vs_exploration.ipynb 
optimiser.maximize(init_points=30, n_iter=30, acq="ei", xi=1e-1)
final_x = np.array(
    [
        optimiser.max["params"][f"dtau{ele}"]
        for ele in range(len(optimiser.max["params"]))
    ]
)

print("Results:")
print(optimiser.max)
print(final_x)


print("\nReward: {}\n".format(penalty))
print("Action: {}\n".format(actions_all))

plt.show()



#Possibility to check pickled data by unserializing it
"""
with open("plots_and_data/actors_mkp_delays_log_data_{}.pickle".format(TIMESTAMP), "rb") as handle:
    data = pickle.load(handle)
print("\nData is: {}".format(data))
"""


