#Small script to extract the current optimised individual and general MKP delays 
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
import pickle
import datetime

#to access the parameter control
from pyjapc import PyJapc
from cern_general_devices import Device
from cern_general_devices.mkp_delays import MKPdelays 
#sys.path.append('/afs/cern.ch/user/e/elwaagaa/cernbox/mkp-waveforms/cern-general-devices/cern_general_devices')  #for VM technical network
#from mkp_delays import MKPdelays
from mkpdelays_env import MKPOptEnv

save_data = True  #if not data has been taken already, set to true 

#Initiate JAPC session
japc = PyJapc('SPS.USER.LHCINDIV', noSet=True)

mkp_env = MKPOptEnv(japc)

TIMESTAMP = str(datetime.datetime.now()).replace(" ", "_").replace(":", "-")

all_vars = []
for i in range(8):
    all_vars.append("MKP.BA1.F3.PFN.{}/ExpertSettingDevice".format(i+1))
all_vars.append("MKP.BA1.F3.CONTROLLER/Setting#arrKickDelay")

if save_data:
    mkp_initial_values = japc.getParam(all_vars)
    print("\nMKP present settings: {}\n".format(mkp_initial_values))

    print("Saving current x0 status...")

    #Save individual and general delay into pickle 
    with open("optimised_mkp_delay_settings/optimised_MKP_delays_{}.pickle".format(TIMESTAMP), "wb") as handle:
        pickle.dump(mkp_initial_values, handle, protocol=pickle.HIGHEST_PROTOCOL)
