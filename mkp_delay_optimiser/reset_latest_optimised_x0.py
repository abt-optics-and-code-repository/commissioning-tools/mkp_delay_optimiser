#Small script to reset the current optimised individual and general MKP delays as of April 11th 2022
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
import pickle
import datetime

#to access the parameter control
from pyjapc import PyJapc
from cern_general_devices import Device
from cern_general_devices.mkp_delays import MKPdelays
from mkpdelays_env import MKPOptEnv

#Initiate JAPC session
japc = PyJapc('SPS.USER.LHCINDIV', noSet=False)

mkp_env = MKPOptEnv(japc)

all_vars = []
for i in range(8):
    all_vars.append("MKP.BA1.F3.PFN.{}/ExpertSettingDevice".format(i+1))
all_vars.append("MKP.BA1.F3.CONTROLLER/Setting#arrKickDelay")


#RESET VALUES TO LATEST OPTIMISED VALUES 
LATEST_OPTIMISED_TIMESTAMP = "optimised_mkp_delay_settings/optimised_MKP_delays_2022-04-22_16-21-56.657127.pickle"
    
with open("{}".format(LATEST_OPTIMISED_TIMESTAMP), "rb") as handle:
    loaded_data = pickle.load(handle)
print("\nLoaded data is: {}\n".format(loaded_data))

#Create a dtau vector and send to the MKP reset 
dtau_reset = np.zeros(9) 
for i in range(8):
    dtau_reset[i] = loaded_data[i]["arrG2MainSwitchFineTimingDelay"][0]
dtau_reset[8] = loaded_data[8][1]  #general time shift 

print(dtau_reset)

#Now set all MKP parameters 
mkp_env.mkp_delays.mkp_delays.set_all_mkp_para(dtau_reset)
