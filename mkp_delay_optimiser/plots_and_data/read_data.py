import pickle
import matplotlib.pyplot as plt
import numpy as np

data = pickle.load(
    open(
        "./actors_mkp_delays_log_data_2022-04-20_10-50-05.998873.pickle", "rb"
    )
)
len(data["out"])
len(data["actions"])

penalty = -1 * np.array(data["out"])

min_index = np.argmin(penalty)
print(min_index)
print(data["actions"][-1])
actions = np.array(data["actions"])

fig, axis = plt.subplots(nrows=2, ncols=1, figsize=(4, 3), sharex=True)
axis[0].set_title("200 ns batch spacing")
axis[0].plot(penalty)
axis[0].axvline(min_index, ls="--", c="r")
axis[0].set(ylabel="Penalty", ylim=(10, 1000))
axis[0].set_yscale("log")
axis[1].plot(actions)
axis[1].set(xlabel="Iterations", ylabel="Actions")
axis[1].legend(np.arange(1, 10))
plt.show()
